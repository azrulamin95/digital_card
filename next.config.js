module.exports = {
  env: {
    BASEURL: process.env.BASEURL,
    MONGODB_URI: process.env.MONGODB_URI,
    MONGODB_DB: process.env.MONGODB_DB
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/groom',
        permanent: true,
      }
    ]
  },
  webpack: (config, { isServer }) => {
    // Fixes npm packages that depend on `fs` module
    if (!isServer) {
      config.node = {
        fs: 'empty'
      }
    }

    return config
  }
}