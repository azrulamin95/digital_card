import { useContext } from 'react';
import { FormGroup, Label, Input  } from 'reactstrap';
import cx from 'classnames';
import { WindowDimensionContext } from '../../contexts/WindowDimension';

export default function InputStandard(props){
    const WindowDimension = useContext(WindowDimensionContext);
    const isMobileView = WindowDimension.width <= 767;
    const { label, type="text", name, id, placeholder, handleOnChange, value } = props;
    return (
        <div style={{flex:1}}>
            <FormGroup>
                <Label for={label} style={{fontWeight:"bold"}}>{label}</Label>
                <Input 
                    className={cx(isMobileView ? "form-control-sm" : "form-control-md")} 
                    type={type} 
                    name={name} 
                    id={id} 
                    placeholder={placeholder}
                    onChange={e => handleOnChange(e.target.value)}
                    value={value}
                />
            </FormGroup>
        </div>
    );
}