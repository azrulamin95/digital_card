import { useState } from 'react';
import { Modal, ModalHeader, ModalBody} from 'reactstrap';

export default function GuestBookModal(props){
    const { 
        isOpen = false 
    } = props;
    const [modal, setModal] = useState(isOpen);
    const toggle = () => setModal(!modal);

    return (
        <div>
            <Modal 
                isOpen={modal} 
                toggle={toggle}
                backdrop={'static'}
            >
                <ModalHeader 
                    toggle={toggle} 
                    style={{
                        paddingBottom: 5, 
                        backgroundColor: "blanchedalmond"
                    }}
                >
                    <span style={{
                        fontFamily: "'Alex Brush', sans-serif",
                        fontWeight: "bold",
                        fontSize: "1.5em"
                    }}>Pengumuman</span>
                </ModalHeader>
                <ModalBody>
                    <span>Majlis akan ditangguhkan sehingga tarikh yang akan diberitahu. 
                    Pihak kami memohon maaf di atas apa-apa kesulitan. 
                    Harap mendoakan semoga dipermudahkan urusan kami.<br/>Terima Kasih.</span>
                </ModalBody>
            </Modal>
        </div>
    );
}
