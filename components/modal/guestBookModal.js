import { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, Card, CardBody, CardTitle, CardText } from 'reactstrap';
import axios from 'axios';

export default function GuestBookModal(props){
  const {
    buttonLabel,
    className,
    modal,
    toggleModal
  } = props;

  const [wishlist, setWishList] = useState(undefined);
  
  useEffect(() => {
    async function fetchWishlistData() {
      try {
        const response = await axios.get('/api/getWishlist');
        setWishList(response.data);  
      } catch (e) {
        console.error(e);
      }
    };
    wishlist === undefined && fetchWishlistData();
  }, []);
  
  const getWishList = () => {
    return wishlist.map((wish, index) => {
      return (
        <div key={index} className={"mb-1"}>
          <Card>
            <CardBody>
              <CardTitle tag="h5">
                <span 
                  style={{
                    fontFamily: "'Alex Brush', sans-serif",
                    fontWeight: "bold",
                    marginBottom: 0,
                    fontSize: "1.1em",
                    textTransform: 'capitalize'
                  }}
                >
                  - {wish.username} -
                </span>
              </CardTitle>
              <CardText><span style={{fontSize: "0.85em"}}>{wish.description}</span></CardText>
            </CardBody>
          </Card>
        </div>
      )
    });
  };

  return (
    <div>
      <Modal isOpen={modal} toggle={toggleModal} 
        className={className} 
        contentClassName={"modal-content-override"} 
        style={{width: "80",backgroundColor:"#F5F5F6",border: "8px double #1C6EA4",borderRadius: "35px 0px 35px 0px"}}
        scrollable={true}
      >
        <ModalHeader toggle={toggleModal}>
          <span style={{
            fontFamily: "'Alex Brush', sans-serif",
            fontWeight: "bold",
            marginBottom: 0,
            fontSize: "2em"
          }}>Ucapan</span>
        </ModalHeader>
        <ModalBody>
          {wishlist && getWishList()}
        </ModalBody>
      </Modal>
    </div>
  );
}
