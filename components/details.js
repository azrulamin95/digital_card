import styles from "../styles/app.module.scss";
import cx from "classnames";
import Header from '../components/text/header';

export default function Details({userDetails}){
    return (
        <div 
            className={cx(styles.homepageContainer,styles.pageWrapper)}
            style={{backgroundImage: `url("/asset/img/frame1.jpg")`}}
        >
            <div className={cx(styles.headerContainer,"mx-5")}>
                <div className={styles.mainHeader}>
                    <h1 style={{fontSize:"3em",marginBottom:8}}>Walimatulurus</h1>
                    <h2 style={{fontWeight:"normal",fontSize:"1.8em"}}>{userDetails.father}</h2>
                    <h2 style={{fontSize:"1.5em"}}>&</h2>
                    <h2 style={{fontWeight:"normal",fontSize:"1.8em"}}>{userDetails.mother}</h2>
                </div>
                <div style={{fontSize:"1em",fontFamily:"none",marginTop:15,marginBottom:15}}>
                    <span>Dengan segala hormatnya mempersilakan tuan/puan ke majlis perkahwinan {userDetails.side == "male"? "putera" : "puteri"} kami</span>
                </div>
                <div className={styles.mainHeader}>
                    <h2 style={{fontWeight:"normal",fontSize:"1.8em"}}>{userDetails.fullname1}</h2>
                    <h2 style={{fontSize:"1.5em"}}>&</h2>
                    <h2 style={{fontWeight:"normal",fontSize:"1.6em"}}>{userDetails.fullname2}</h2>
                </div>
                <div style={{fontFamily:"none"}}>
                    <Header styles={styles} headerText="Tempat">
                        <span>{userDetails.add1}<br />{userDetails.add2}{" "}{userDetails.add3}</span>
                    </Header>
                    <Header styles={styles} headerText="Tarikh">
                        <span>{userDetails.day}, {userDetails.date}<br />{userDetails.islamic}</span>
                    </Header>
                    <Header styles={styles} headerText="Masa">
                        <span>{userDetails.time}</span>
                    </Header>
                </div>
            </div>
        </div>
    );
};