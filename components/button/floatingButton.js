import { useState } from 'react';
import FloatingOptions from "./floatingOptions";
import GuestBookModal from "../modal/guestBookModal";

export default function FloatingButton({location}){
    const [modal, setModal] = useState(false);
    const toggleModal = () => setModal(!modal);
    
    return (
        <>
            <FloatingOptions toggleModal={toggleModal} location={location}/>
            <GuestBookModal modal={modal} toggleModal={toggleModal}/>
        </>
    );
}