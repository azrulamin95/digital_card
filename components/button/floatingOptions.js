export default function FloatingOptions(props){
    const { toggleModal, location } = props;
    const coordinate = location.split(",");

    return (
        <div className="menu pmd-floating-action" role="navigation"> 
            <a 
                href={`https://maps.google.com/?q=${location}&ll=${location}&z=17`} 
                className="pmd-floating-action-btn btn pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default" 
                data-title="Google Maps"
            >
                <span className="pmd-floating-hidden">G Maps</span>
                <i><img src="https://img.icons8.com/bubbles/50/000000/google-maps.png" style={{width:50,height:50}}/></i>
            </a>
            <a 
                href={`https://www.waze.com/ul?ll=${coordinate[0]}%2C${coordinate[1]}&navigate=yes&zoom=17`} 
                target="_blank" 
                className="pmd-floating-action-btn btn pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default" 
                data-title="Waze"
            >
                <span className="pmd-floating-hidden">Waze</span> 
                <i><img src="https://img.icons8.com/color/48/000000/waze.png" style={{width:50,height:50}} /></i> 
            </a> 
            <a  
                className="pmd-floating-action-btn btn pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default bg-grey" 
                data-title="Guestbook" 
                onClick={(e)=>{
                    e.preventDefault();
                    toggleModal();
                }}
            >
                <span className="pmd-floating-hidden">Guestbook</span> 
                <i className="material-icons" style={{width:50,height:50}}>menu_book</i>
            </a> 
            <a 
                className="pmd-floating-action-btn btn pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-primary" 
                onClick={e => e.preventDefault()}
            > 
                <span className="pmd-floating-hidden">Primary</span>
                <i className="material-icons pmd-sm">more_vert</i> 
            </a>
            <style jsx>{`
                .bg-grey {background-color: lightgrey}
                .pmd-floating-action { bottom: 10px; position: fixed;  margin:1em;  right: 0; }
                .pmd-floating-action-btn { display:block; position: relative; transition: all .2s ease-out;}
                .pmd-floating-action-btn:before { bottom: 10%; content: attr(data-title); opacity: 0; position: absolute; right: 100%; transition: all .2s ease-out .5s;  white-space: nowrap; background-color:#fff; padding:6px 12px; border-radius:2px; color:#333; font-size:12px; margin-right:5px; display:inline-block; box-shadow: 0px 2px 3px -2px rgba(0, 0, 0, 0.18), 0px 2px 2px -7px rgba(0, 0, 0, 0.15);}
                .pmd-floating-action-btn:last-child:before { font-size: 14px; bottom: 25%;}
                .pmd-floating-action-btn:active, .pmd-floating-action-btn:focus, .pmd-floating-action-btn:hover {
                    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);
                }
                .pmd-floating-action-btn:not(:last-child){ opacity: 0; -ms-transform: translateY(20px) scale(0.3); transform: translateY(20px) scale(0.3); margin-bottom:15px; margin-left:8px; position:absolute; bottom:0;}
                .pmd-floating-action-btn:not(:last-child):nth-last-child(1) { transition-delay: 50ms;}
                .pmd-floating-action-btn:not(:last-child):nth-last-child(2) { transition-delay: 100ms;}
                .pmd-floating-action-btn:not(:last-child):nth-last-child(3) { transition-delay: 150ms;}
                .pmd-floating-action-btn:not(:last-child):nth-last-child(4) { transition-delay: 200ms;}
                .pmd-floating-action-btn:not(:last-child):nth-last-child(5) { transition-delay: 250ms;}
                .pmd-floating-action-btn:not(:last-child):nth-last-child(6) { transition-delay: 300ms;}
                .pmd-floating-action:hover .pmd-floating-action-btn, .menu--floating--open .pmd-floating-action-btn { 
                    opacity: 1; 
                    -ms-transform: none; 
                    transform: none; 
                    position:relative; 
                    bottom:auto;
                }
                .pmd-floating-hidden{ display:none;}
                .pmd-floating-action-btn.btn:hover{ overflow:visible;}
                .pmd-floating-action-btn .ink{ width:50px; height:50px;}
            `}</style>
        </div>
    );
}