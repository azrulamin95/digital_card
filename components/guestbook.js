import { useEffect, useContext, useState } from 'react';
import styles from "../styles/app.module.scss";
import cx from "classnames";
import Header from "../components/text/header";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";
import InputStandard from "../components/form/inputStandard";
import { Button } from 'reactstrap';
import { WindowDimensionContext } from '../contexts/WindowDimension';
import axios from 'axios';

export default function Guestbook({contact}){
    const [wishlist, setWishList] = useState(undefined);
    const WindowDimension = useContext(WindowDimensionContext);
    const isMobileView = WindowDimension.width <= 767;
    const [username,setUsername] = useState("");
    const [desc,setDesc] = useState("");
    
    const fetchWishlistData = async () => {
        try {
            const response = await axios.get('/api/getWishlist?limit=10');
            setWishList(response.data);
        } catch (e) {}
    };

    const handleSubmit = async () => {
        if(username !== "" && desc !== ""){
            await axios.post('/api/postWishlist', {
                username: username,
                description: desc
            });
            setUsername("");
            setDesc("");

            fetchWishlistData();
        }
    };

    useEffect(() => {
        wishlist === undefined && fetchWishlistData();
    },[]);

    const handlePhoneNumber = number => {
        let phone = number.replace("60","");
        return `${phone.slice(0, 3)}-${phone.slice(3)}`;
    };
    
    return (
        <div 
            className={cx(styles.homepageContainer,styles.pageWrapper)}
            style={{backgroundImage: `url("/asset/img/frame1.jpg")`}}
        >
            <div className={cx(styles.headerContainer, "mx-5")}>
                <div className={styles.mainHeader}>
                    <h1 style={{fontSize:"3em",marginBottom:8}}>Ruangan Tetamu</h1>
                </div>
                <div style={{fontFamily:"none"}}>
                    <Header styles={styles} headerText="Hubungi:" defaultClass="mt-3">
                        <div className={"d-flex justify-content-center text-left font-weight-bold"} style={{fontSize:"1.1em"}}>
                            <div style={{width:"fit-content"}}>
                            {
                                Object.entries(contact).map(([ person, number ], index) => {
                                    return (
                                        <div key={index}>
                                            <span>{person}: <a href={`tel:${number}`}>{handlePhoneNumber(number)}</a></span><br/>
                                        </div>
                                    )
                                })
                            }
                            </div>
                        </div>
                    </Header>
                </div>
                <br />
                <div className={styles.wishSliderContainer}>
                    <Header styles={styles} headerText="Ucapan:" defaultClass="mt-2">
                        {wishlist !== undefined && (
                            <Slider className={styles.sliderStyle} infinite={true} autoplay={3000}>
                                { 
                                    wishlist.map((slide, index) => 
                                        <div key={index}>
                                            <div style={{width:"80%",margin:"0 auto"}}>
                                                <h3 className={styles.sliderTextHeader}>- {slide.username} -</h3>
                                                <span>{slide.description}</span>
                                            </div>
                                        </div>
                                    )
                                }
                            </Slider>
                        )}
                    </Header>
                </div>
                <div>
                    <Header styles={styles} headerText="Tinggalkan Ucapan:" defaultClass="mt-3">
                        <div className="mt-2" style={{width: isMobileView ? "90%" : "60%",textAlign:"left",margin:"auto"}}>
                            <InputStandard 
                                label="NAMA" 
                                type="text" 
                                name="name" 
                                id="name"  
                                placeholder="Sila masukkan nama." 
                                handleOnChange={setUsername}
                                value={username}
                            />
                            <InputStandard 
                                label="Ucapan" 
                                type="textarea" 
                                name="ucapan" 
                                id="ucapan" 
                                placeholder="Sila tinggalkan ucapan." 
                                handleOnChange={setDesc}
                                value={desc}    
                            />
                            <Button onClick={handleSubmit} color="primary" size={isMobileView ? "sm" : "md"} block>Hantar</Button>
                        </div>
                    </Header>
                </div>
            </div>
        </div>
    );
};