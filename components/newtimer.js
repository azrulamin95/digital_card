import { useState, useEffect } from 'react';
import moment from 'moment';
import styles from '../styles/countdown.module.scss';
import cx from "classnames";

export default function NewTimer(props){
    // Stackoverflow: https://stackoverflow.com/questions/10756313/javascript-jquery-map-a-range-of-numbers-to-another-range-of-numbers
    const mapNumber = (number, in_min, in_max, out_min, out_max) => {
        return (number - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    };

    const SVGCircle = ({ radius }) => {
        let newRadius = radius ? radius : 0;
        return (
            <svg className={styles.countdownSvg}>
                <path fill="none" stroke="#333" strokeWidth="4" d={describeArc(50, 50, 48, 0, newRadius)}/>
            </svg>
        )
    };

    // From stackoverflow: https://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
    const polarToCartesian = (centerX, centerY, radius, angleInDegrees) => {
        let angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
    
        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    };
  
    const describeArc = (x, y, radius, startAngle, endAngle) => {
        let start = polarToCartesian(x, y, radius, endAngle);
        let end = polarToCartesian(x, y, radius, startAngle);

        let largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

        let d = [
            "M", start.x, start.y, 
            "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");

        return d;       
    };

    let countdown = {
        days: undefined,
        hours: undefined,
        minutes: undefined,
        seconds: undefined
    };
    
    const [timerCount,setTimerCount] = useState(countdown);
    const { timeTillDate, timeFormat } = props;
    const then = moment(timeTillDate, timeFormat);
    
    useEffect(() => {
        setInterval(() => {
            const now = moment();
            const duration = moment.duration(then.diff(now));
            const days = Math.floor(duration.asDays());
            const hours = duration.hours();
            const minutes = duration.minutes();
            const seconds = duration.seconds();
            setTimerCount({ days, hours, minutes, seconds });
		}, 1000);
    },[]);

    const { days, hours, minutes, seconds } = timerCount;

    // const daysRadius = mapNumber(days, 30, 0, 0, 360);
    const hoursRadius = mapNumber(hours, 24, 0, 0, 360);
    const minutesRadius = mapNumber(minutes, 60, 0, 0, 360);
    const secondsRadius = mapNumber(seconds, 60, 0, 0, 360);
    return (
        <div 
            className={cx(styles.countdownContainer, styles.pageWrapper)}
            style={{backgroundImage: `url("https://previews.123rf.com/images/foodandmore/foodandmore1710/foodandmore171000119/88449792-designer-wedding-rings-in-the-corner-on-a-sparkling-glitter-background-in-panoramic-banner-format-wi.jpg")`}}
        >
            <h1 style={{textAlign:"center",fontFamily:"'Alex Brush', sans-serif",fontSize:"3em",fontWeight:"bolder"}}>Countdown</h1>
            <div className={styles.countdownWrapper}>
                <div className={cx(styles.countdownItem,styles.countdownItemResponsive)}>
                    <SVGCircle radius={days} />
                    {days} 
                    <span>hari</span>
                </div>
                <div className={cx(styles.countdownItem,styles.countdownItemResponsive)}>
                    <SVGCircle radius={hoursRadius} />
                    {hours} 
                    <span>jam</span>
                </div>
                <div className={cx(styles.countdownItem,styles.countdownItemResponsive)}>
                    <SVGCircle radius={minutesRadius} />
                    {minutes} 
                    <span>minit</span>
                </div>
                <div className={cx(styles.countdownItem,styles.countdownItemResponsive)}>
                    <SVGCircle radius={secondsRadius} />
                    {seconds} 
                    <span>saat</span>
                </div>
            </div>
        </div>
    );
}