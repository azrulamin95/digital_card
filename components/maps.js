import styles from '../styles/app.module.scss';

export default function Maps({location}){
    return (
        <div className={styles.pageWrapper}>
            <iframe 
                className={styles.pageWrapper} 
                src={`https://maps.google.com/maps?q=${location}&hl=en&z=14&output=embed`}
                frameBorder="0" 
                allowFullScreen=""
                style={{height: "50vh"}}
            />
        </div>
    );
}