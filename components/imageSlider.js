import React, { useState } from "react";

const ImageSlider = (props) => {
    const { setHomePageImg } = props;
    const images = [
        'lelaki.jpg',
        'temp1-1.jpg',
        'temp1.jpg',
        'temp2.jpg',
        'temp3.jpg',
        'temp4.jpg',
        'temp5.jpg',
        'temp6.jpg',
    ];

    const [index, setIndex] = useState(0);
    const slideRight = () => {
        setIndex((index + 1) % images.length);
    };

    const slideLeft = () => {
        const nextIndex = index - 1;
        if (nextIndex < 0) {
            setIndex(images.length - 1);
        } else {
            setIndex(nextIndex);
        }
    };

    const handleOnclick = () => {
        setHomePageImg(images[index]);
    };

    return (
        images.length > 0 && (
            <div style={{position:"absolute",right:0,top:0}}>
                <img src={`/asset/img/${images[index]}`} alt={index} height="150"/>
                <div>
                    <button onClick={slideLeft}>{"<"}</button>
                    <button onClick={handleOnclick}>{"choose"}</button>
                    <button onClick={slideRight}>{">"}</button>
                </div>
            </div>
        )
    );
};

export default ImageSlider;