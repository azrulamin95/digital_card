import styles from "../styles/app.module.scss";

export default function Prayer(){
    return (
        <div className={styles.pageWrapper}>
            <div className={styles.prayerContainer}>
                <span>
                    Segala puji hanya untuk-Mu ya Allah atas segala nikmat pemberian yang Engkau kurniakan kepada kami.
                </span><br/><br/>
                <span>
                    Ya Allah, pada hari yang berkat ini, kami memohon rahmat-Mu ya Allah, 
                    kurniakanlah kepada kedua mempelai yang diraikan ini kehidupan yang sihat dan selamat serta 
                    dikurniakan zuriat yang soleh dan solehah. Perkukuhkanlah ikatan kasih sayang dan hormat menghormati antara mereka, 
                    berakhlak mulia dan berbudi bahasa, serta beriman dan bertakwa kepada-Mu ya Allah pada setiap masa.
                </span><br/><br />
                <span style={{fontSize:"2.3em", fontFamily: "'Alex Brush', sans-serif"}}>Amin...</span>
            </div>
        </div>
    );
};