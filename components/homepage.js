import { useState } from "react";
import styles from "../styles/app.module.scss";
import ImageSlider from "../components/imageSlider";
import Header from '../components/text/header';

export default function Homepage({showEditor, userDetails}){
    const [homePageImg,setHomePageImg] = useState('temp6.jpg');
    return (
        <div className={styles.pageWrapper}>
            {showEditor && <ImageSlider setHomePageImg={setHomePageImg}/>}
            <div 
                className={styles.homepageContainer}
                style={{backgroundImage: `url("/asset/img/${homePageImg}")`}}
            >
                <div className={styles.headerContainer}>
                    <div className={styles.mainHeader}>
                        <h1>Walimatulurus</h1>
                        <h2>{userDetails.username1}</h2>
                        <h2 style={{fontSize:"1.8em"}}>&</h2>
                        <h2>{userDetails.username2}</h2>
                    </div>
                    <br />
                    <div className="mt-4">
                        <Header styles={styles} headerText="Tarikh">
                            <div className={styles.detailsInformation}>
                                <span>{userDetails.day}, {userDetails.date}</span>
                            </div>
                        </Header>
                        <Header styles={styles} headerText="Tempat">
                            <div className={styles.detailsInformation}>
                                <span>{userDetails.add1}</span><br />
                                <span>{userDetails.add2}</span><br />
                                <span>{userDetails.add3}</span>
                            </div>
                        </Header>
                    </div>
                </div>
            </div>
        </div>
    );
};