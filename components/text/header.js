export default function Header(props){
    const { 
        styles,
        headerText,
        defaultClass = "mt-4" 
    } = props;
    return (
        <div className={defaultClass}>
            <h3 className={styles.headerInformation}>{headerText}</h3>
            {props.children}
        </div>
    );
}