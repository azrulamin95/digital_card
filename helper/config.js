export const arrayToObj = (data) => {
    return data.reduce((a, b) => Object.assign(a, b), {});
};