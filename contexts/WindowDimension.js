import React, { useEffect, useState } from "react";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

export default function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState({});

  useEffect(() => {
    setWindowDimensions(getWindowDimensions());
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}

export const WindowDimensionContext = React.createContext();
export const WindowDimensionProvider = props => (
  <WindowDimensionContext.Provider value={useWindowDimensions()}>
    {props.children}
  </WindowDimensionContext.Provider>
);

export const withWindowDimension = Component => () => {
  <WindowDimensionContext.Consumer>
    {props => <Component {...props} />}
  </WindowDimensionContext.Consumer>;
};
