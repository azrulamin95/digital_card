import Head from 'next/head'
import axios from 'axios';
import { useState } from 'react';
import styles from '../styles/Home.module.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Homepage from '../components/homepage';
import Details from '../components/details';
import Prayer from '../components/prayer';
import Guestbook from '../components/guestbook';
import NewTimer from '../components/newtimer';
import Maps from "../components/maps";
import FloatingButton from "../components/button/floatingButton";
import Modal from "../components/modal/modal";
import { arrayToObj } from "../helper/config";

export default function Home(props) {
  const [showEditor, setShowEditor] = useState(props.editFlag);
  const userDetails = arrayToObj(props.userDetails);
  const { annoucement } = userDetails;
  
  return (
    <div className={styles.container}>
      <Head>
        <title>{userDetails.username1} | {userDetails.username2} Wedding Invitation</title>
        <link rel="icon" href="/asset/icon/wed_icon.png" />
        <link href="https://fonts.googleapis.com/css2?family=Alex+Brush&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        <link href="https://propeller.in/components/icons/css/google-icons.css" rel="stylesheet" />
        <link href="https://propeller.in/components/typography/css/typography.css" rel="stylesheet" />
        <link href="https://propeller.in/components/button/css/button.css" rel="stylesheet" />
      </Head>
      <Homepage 
        showEditor={showEditor}
        userDetails={userDetails}
      />
      <Details userDetails={userDetails} />
      <NewTimer 
        timeTillDate={userDetails.countdown}
		    timeFormat="MM DD YYYY, h:mm a"
      />
      <Prayer />
      <Maps location={userDetails.location}/>
      <Guestbook contact={userDetails.contact}/>
      <FloatingButton location={userDetails.location}/>
      <Modal isOpen={annoucement}/>
    </div>
  )
}

export async function getServerSideProps(props){
  const editFlag = props.query.edit == "true";
  const future = props.query.future || 'groom';

  const userDetails = await axios.get(`/getUserDetails?future=${future}`);
  
  return {
    props: {
      editFlag: editFlag,
      userDetails: userDetails.data
    }
  }
}