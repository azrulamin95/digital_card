import { connectToDatabase } from '../../utils/mongodb';

export default async (req,res) => {
    const body = req.body;
    const { db } = await connectToDatabase();

    await db.collection("wishlist").insertOne(body, function(err, res) {
        if (err) throw err;
    });
    res.status = 200;
    res.json(true);
};