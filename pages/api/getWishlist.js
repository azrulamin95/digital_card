import { connectToDatabase } from '../../utils/mongodb';

export default async (req,res) => {
    let { limit } = req.query;
    const { db } = await connectToDatabase();

    let wish = db.collection("wishlist").find();

    if (limit !== ""){
        wish = wish.limit(parseInt(limit));
    }

    const wishlist = await wish.toArray();
    res.json(wishlist);
};