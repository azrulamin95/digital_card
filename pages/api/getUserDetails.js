import { connectToDatabase } from '../../utils/mongodb';

export default async (req,res) => {
    const { future } = req.query;
    const { db } = await connectToDatabase();
    let details = db.collection("user_details").find({user_id: future});

    const user = await details.toArray();
    res.json(user);
};