import '../styles/globals.css'
import { WindowDimensionProvider } from "../contexts/WindowDimension";
import { withCookies } from "react-cookie";
import nookies from "nookies";
import axios from 'axios';

function MyApp(props) {
  let {
    Component,
    pageProps
  } = props;

  return (
    <WindowDimensionProvider>
      <Component {...pageProps} />
    </WindowDimensionProvider>
  );
}

MyApp.getInitialProps = async ({ Component, ctx, res }) => {
  const isServer = !!ctx.req;
  const asPath = ctx.asPath,
    pathname = ctx.pathname;
  const cookies = isServer ? nookies.get(ctx) : nookies.get();

  let pageProps = {};
  axios.defaults.baseURL = process.env.BASEURL + '/api';

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(
      ctx
    );
  }

  return {
    pageProps,
    asPath
  };
};

export default withCookies(MyApp);
